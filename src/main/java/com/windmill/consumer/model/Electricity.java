package com.windmill.consumer.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table(name = "electricity")
@Data
public class Electricity implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6576324953564602096L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "mill_id")
	private String millId;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "id")
	private WindMill mill;

	@Column(name = "electricity_charge")
	private Double electricityCharge;

	@Column(name = "is_active")
	private Boolean isActive=false;

	@Column(name = "created_at")
	private LocalDateTime createdAt;

	@Column(name = "updated_at")
	private LocalDateTime updatedAt;


}
