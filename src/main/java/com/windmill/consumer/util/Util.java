package com.windmill.consumer.util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Util {

	private Util() {

	}
	
	public static String getCurrentDateTimeString() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
	}
	
	public static LocalDateTime getStringCurrentDateTime(String datetime) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime dateTime = LocalDateTime.parse(datetime, formatter);
		return dateTime;
	}

	public static LocalDateTime getCurrentDateTime() {
		return LocalDateTime.now();
	}

}