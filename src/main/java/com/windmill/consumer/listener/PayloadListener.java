package com.windmill.consumer.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.windmill.consumer.dto.PushMessagePayload;
import com.windmill.consumer.service.ElectricityService;

@Service
public class PayloadListener {

	private static final Logger logger = LoggerFactory.getLogger(PayloadListener.class);

	@Autowired
	private ElectricityService electricityService;

	@RabbitListener(queues = "${queue.api_message}")
	public void processPayloadMessage(PushMessagePayload payload) {
		logger.info("Payload :::: {}", payload);
		electricityService.addPayload(payload);
	}

}
