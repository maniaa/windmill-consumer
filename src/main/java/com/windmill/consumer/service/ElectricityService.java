package com.windmill.consumer.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.windmill.consumer.dto.PushMessagePayload;
import com.windmill.consumer.model.Electricity;
import com.windmill.consumer.repo.ElectricityRepo;
import com.windmill.consumer.util.Util;

@Service
public class ElectricityService {
	
	@Autowired
	ElectricityRepo electricityRepo;

	private static final Logger LOGGER = LoggerFactory.getLogger(ElectricityService.class);

	public void addPayload(PushMessagePayload message) {
		try {
			Electricity electricity = new Electricity();
			electricity.setMillId(message.getMillId());
			electricity.setElectricityCharge(message.getElectricityCharge());
			electricity.setCreatedAt(Util.getStringCurrentDateTime(message.getDatetime()));
			electricityRepo.save(electricity);
		} catch (Exception ex) {
			LOGGER.error("Error in Calling Firebase,", ex);
		}
	}

}
