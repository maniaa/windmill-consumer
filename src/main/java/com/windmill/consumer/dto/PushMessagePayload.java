package com.windmill.consumer.dto;

import lombok.Data;

@Data
public class PushMessagePayload {

	private String millId;

	private Double electricityCharge;

	private String datetime;

}
