package com.windmill.consumer.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class ApplicationProperties {

	@Autowired
	private Environment env;

	public String getFirebaseKey() {
		return env.getProperty("firebase_key");
	}
	
	public String getAPNSCertPath() {
		return env.getProperty("APNS_cert_path");
	}
	
	public String getAPNSCertPass() {
		return env.getProperty("APNS_cert_pass");
	}

}
