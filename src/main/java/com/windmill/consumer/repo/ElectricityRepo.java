package com.windmill.consumer.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.windmill.consumer.model.Electricity;;

@Repository
public interface ElectricityRepo extends JpaRepository<Electricity, Long> {


}
