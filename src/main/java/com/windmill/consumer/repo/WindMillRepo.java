package com.windmill.consumer.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.windmill.consumer.model.WindMill;

@Repository
public interface WindMillRepo extends JpaRepository<WindMill, String> {

	WindMill findTopByOrderByCreatedAtDesc();

}
